/// <reference types="Cypress" />

describe("Tickets", () => {
  beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

  it("Digita em todos os campos Iputs da página", () => {
    const firstName = "Makson";
    const lastName = "Rocha";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("makson@gmail.com");
    cy.get("#requests").type("Gosta de Estudar");
    cy.get("#signature").type(`${firstName} ${lastName}`);
  });

  it("Selecionar o numero 2 no select", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("Selecionar vip no radio button", () => {
    cy.get("#vip").check();
  });

  it("Selecionar friend e publication no checkbox", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
  });

  it("Selecionar friend e publication depois desmacar publication e marcar social-media no checkbox", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#publication").uncheck();
    cy.get("#social-media").check();
  });

  it("Verificar se existe o titulo 'TICKETBOX' no cabecalho da pagina", () => {
    // Validação
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("Verifica campo obrigatorio quando digitar um email invalido", () => {
    cy.get("#email").as("email").type("makson-gmail.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email").clear().type("makson@gmail.com");

    // Validação
    cy.get("#email.invalid").should("not.exist");
  });

  it("Teste, preencher todos os campos e depois resetar", () => {
    const firstName = "Makson";
    const lastName = "Rocha";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("makson@gmail.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#social-media").check();
    cy.get("#requests").type("Gosta de Estudar");

    // Validação
    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets. `
    );

    cy.get("#agree").click();
    cy.get("#signature").type(`${fullName}`);

    // Validação
    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[type='submit']").click();

    // Validação
    cy.get("@submitButton").should("be.disabled");
  });

  it("Teste e2e com comandos personalizados", () => {
    const customer = {
      firstName: "João",
      lastName: "Rocha",
      email: "joao@gmail.com",
    };

    cy.fillMandatoryFields(customer);

    // Validação
    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();

    // Validação
    cy.get("@submitButton").should("be.disabled");
  });
});
